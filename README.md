LogPie
======

**LogPie** 是一个通过分析日志(*Console logs*)进行故障预测的工具集。

**LogPre** 是 **LogPie** 使用Python搭建的原型,

一旦设计稳定, 代码会逐渐迁移至Elixir/Erlang, 并汇入这里.

**LogPie/LogPre** 优先托管于GitLab, Issue/Wiki也将生长于此.

GitHub上另有一份 **仅含代码** 的镜像.
